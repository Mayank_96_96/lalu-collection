// import $ from 'jquery';
import slick from "slick-carousel";
import { formatMoney } from '@shopify/theme-currency';
export default () => {
    ((custom, $) => {
        const $dom = {};

        const cacheDom = () => {
            $dom.bodyElement = $("body");
        };
        const bindUIActions = () => {

            $(document).ready(function() {
                var range = $("#watch").data("range");
                if (range > 0) {
                    var x = document.getElementById("watch");
                    x.innerHTML = Math.floor((Math.random() * range) + 1);
                }
                $(".watch-btn").click(function() {
                    var src = $(this).data("src");
                    $(".popup-youtube-player").attr("src", src);
                });
                $("#mymodel").on('hidden.bs.modal', function() {
                    $("#mymodel iframe").attr("src", $("#mymodel iframe").attr("src"));
                });
            });
            $(".toggle-btn").click(function() {
                // setInterval(function () {
                //     $(".order-popup-mian").removeClass("hide");
                //     $(".order-popup-mian").addClass("show");
                //     var max = 50;
                //     var min = 1;
                //     var ramdomvalue = Math.floor(Math.random() * (max - min + 1) + min);
                //     $.ajax({
                //         type: "GET",
                //         url: "/admin/api/2019-07/orders.json",
                //         dataType: "json",
                //         username: "c99212b20f563ed22b302abf0f0fcc7a",
                //         password: "20a444538195c453d29b4ddff17e9e64",
                //         crossDomain: true,
                //         async: true,
                //         success: function (data) {
                //             var C_name = data.orders[ramdomvalue].customer.first_name;
                //             $('#c_name').text(C_name);
                //         },
                //         error: function () {}
                //     });
                // }, 10000);

                // $(".close-btn").click(() => {
                //     $(".order-popup-mian").removeClass("show");
                //     $(".order-popup-mian").addClass("hide");
                // });
            });
            $(".toggle-btn").click(function() {
                $(".mobile-menu").css({
                    left: "0%"
                });
            });
            $(".close-btn").click(function() {
                $(".mobile-menu").css({
                    left: "-100%"
                });
            });

            $(".shop").click(function() {
                $(".learn").removeClass('is-active');
                $(this).addClass('is-active');
                $('.mobile-learnmenu-div').css({
                    right: "-100%"
                });
                $('.mobile-menu-div').css({
                    left: "0"
                });
            });
            $(".learn").click(function() {
                $(".shop").removeClass('is-active');
                $(this).addClass('is-active');
                $('.mobile-menu-div').css({
                    left: "-100%"
                });
                $('.mobile-learnmenu-div').css({
                    right: "0"
                });
            });
            // if ($(".shop").hasClass("is-active")){

            // });

            // if ($(".learn").hasClass("is-active")){
            //     $('.mobile-menu-div').css({ left: "-100%" });
            //     $('.mobile-learnmenu-div').css({ right: "0" });
            // });

            $('.single-checkbox').on('change', function() {
                if ($('.single-checkbox:checked').length > 2) {
                    this.checked = false;
                }
            });

            // function first_step() {
            //     $(".step").removeClass("active");
            //     $("#question_1").addClass("active");
            //     $(".setup-content-1").css("display", "block");
            //     $(".setup-content-2").css("display", "none");
            //     $(".setup-content-3").css("display", "none");
            //     $(".coupan_code").css("display", "none");
            // }

            function sec_step() {
                $(".step").removeClass("active");
                $("#question_1").addClass("active");
                $("#question_2").addClass("active");
                $(".setup-content-1").css("display", "none");
                $(".setup-content-2").css("display", "block");
                $(".setup-content-3").css("display", "none");
                $(".coupan_code").css("display", "none");
            }

            function third_step() {
                $(".step").removeClass("active");
                // $("#question_1").addClass("active");
                $("#question_2").addClass("active");
                $("#coupan").addClass("active");
                // $(".setup-content-1").css("display", "none");
                $(".setup-content-2").css("display", "none");
                $(".setup-content-3").css("display", "block");
                $(".coupan_code").css("display", "block");
            }
            // $("#question_1").click(function() {
            //     first_step();
            // });

            $("#question_2").click(function() {
                // var answerValue = $("input:checked").length;
                // if (answerValue == 0) {

                // } else {
                //     sec_step();
                //     $(".error_2").css("display", "none");
                // }
                sec_step();
                // var answerValue = $("#answer_2_text").val().length;
                // if (answerValue != 0 || $("#answer_1_text").val().length != 0) {
                //     sec_step();
                // }
            });
            $("#coupan").click(function() {
                var answerValue = $("input:checked").length;
                var email = $('#k_id_email').val().length;
                var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
                if (answerValue == 0 || email == 0 || (!$('#k_id_email').val().match(mailformat))) {

                } else {
                    third_step();
                }
                // var answerValue = $("#answer_2_text").val().length;
                // if (answerValue != 0 || $("#answer_1_text").val().length != 0) {
                //     third_step();
                // }
            });

            // $("#button_1").click(function() {
            //     $(".error_1").css("display", "none");
            //     var email = $("#email").val();
            //     $(".step").removeClass("active");
            //     if (email) {
            //         $(".title_main").css("display", "block");
            //         $("#main_content").css("display", "none");
            //         $("#progressbar").css("display", "block");
            //         first_step();
            //     } else {
            //         $(".error_1").css("display", "block");
            //     }
            // });
            $("#button_3").click(function() {
                $(".email_error").css("display", "none");
                $(".error_3").css("display", "none");
                $(".error_2").css("display", "none");
                var answerValue1 = $("input:checked").length;
                var answerValue = $("#k_id_email").val().length;
                var mailformat = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$");
                var e = $('#k_id_email').val();
                console.log(mailformat.test(e));
                if (answerValue == 0) {
                    $(".error_3").css("display", "block");
                    $(".email_error").css("display", "none");
                } else if (!mailformat.test(e)) {
                    $(".error_3").css("display", "none");
                    $(".email_error").css("display", "block");
                } else if (answerValue1 == 0) {
                    $(".error_2").css("display", "block");
                } else {
                    third_step();
                }

                // var answerValue = $("input:checked").length;
                // if (answerValue == 0) {
                //     $(".error_2").css("display", "block");
                // } else {
                //     sec_step();
                //     $(".error_2").css("display", "none");
                // }

            });
            // $("#button_3").click(function() {
            //     var answerValue = $("#email").val().length;
            //     var mailformat = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$");
            //     var e = $('#email').val();
            //     console.log(mailformat.test(e));
            //     if (answerValue == 0) {
            //         $(".error_3").css("display", "block");
            //         $(".email_error").css("display", "none");
            //     } else if (!mailformat.test(e)) {
            //         $(".error_3").css("display", "none");
            //         $(".email_error").css("display", "block");
            //     } else {
            //         third_step();
            //         $(".email_error").css("display", "none");
            //         $(".error_3").css("display", "none");
            //     }
            //     // $(".error_3").css("display", "none");
            //     // var answerValue = $("#answer_2_text").val().length;
            //     // if (answerValue != 0) {
            //     //     third_step();
            //     // } else {
            //     //     $(".error_3").css("display", "block");
            //     // }
            // });

            function first_tab() {
                $(".quiz-navigation-item").removeClass("is-active");
                $("#first").addClass("is-active");
                $(".quiz-1").css("display", "block");
                $(".quiz-2").css("display", "none");
                $(".quiz-3").css("display", "none");
                $(".quiz-4").css("display", "none");
            }

            function sec_tab() {
                $(".quiz-navigation-item").removeClass("is-active");
                $("#sec").addClass("is-active");
                $(".quiz-1").css("display", "none");
                $(".quiz-2").css("display", "block");
                $(".quiz-3").css("display", "none");
                $(".quiz-4").css("display", "none");
            }

            function third_tab() {
                $(".quiz-navigation-item").removeClass("is-active");
                $("#third").addClass("is-active");
                $(".quiz-1").css("display", "none");
                $(".quiz-2").css("display", "none");
                $(".quiz-3").css("display", "block");
                $(".quiz-4").css("display", "none");
            }

            function forth_tab() {
                $(".quiz-navigation-item").removeClass("is-active");
                $("#forth").addClass("is-active");
                $(".quiz-1").css("display", "none");
                $(".quiz-2").css("display", "none");
                $(".quiz-3").css("display", "none");
                $(".quiz-4").css("display", "block");
            }
            $("#first").click(function() {
                $(".quiz-navigation-item").removeClass("is-active");
                $("#first").addClass("is-active");
                var radioValue = $("input[name='option']:checked").val();
                if (radioValue) {
                    first_tab();
                }
                $(".product_list").css("display", "none");
            });
            $("#sec").click(function() {
                var radioValue = $("input[name='option']:checked").val();
                if (radioValue || $(".single-checkbox:checked").length == 2) {
                    sec_tab()
                }

            });
            $("#third").click(function() {
                if ($("#email").val() != "") {
                    third_tab();
                }
                $(".product_list").css("display", "none");
            });
            $("#forth").click(function() {
                if ($("#email").val() != "") {
                    forth_tab();
                }
            });
            $("#btn-1").click(function() {
                var radioValue = $("input[name='option']:checked").val();
                if (radioValue == null) {
                    $(".error_1").css("display", "block");
                } else {
                    sec_tab();
                    $(".error_1").css("display", "none");
                }
            });

            $("#btn-2").click(function() {

                if ($(".single-checkbox:checked").length == 2) {
                    third_tab();
                    $(".error_2").css("display", "none");
                } else {
                    $(".error_2").css("display", "block");
                }
            });

            $("#btn-3").click(function() {
                var radioValue = $("input[name='option']:checked").val();
                var email = $("#email").val();
                if ($("#email").val()) {
                    forth_tab();
                    $(".error_3").css("display", "none");
                    $(".product_list").each(function(i, item) {
                        var tags = $(item).data("tags");
                        if (tags.indexOf(radioValue) >= 0) {
                            $("#" + item.id).css("display", "block");
                        }
                        var elements = document.getElementsByName("optionArray[]");
                        for (var i = 0; i < elements.length; i++) {
                            if (elements[i].checked) {
                                if (tags.indexOf(elements[i].value) >= 0) {
                                    $("#" + item.id).css("display", "block");
                                }
                            }
                        }
                    });
                } else {
                    $(".error_3").css("display", "block");
                }

            });

            var $button = $('.back-to-top'),
                windowH = $(window).height();
            if ($(window).scrollTop() > windowH / 2) {
                $button.addClass('is-visible');
            }
            $(window).scroll(function() {
                if ($(this).scrollTop() > windowH / 2) {
                    $button.addClass('is-visible');
                } else {
                    $button.removeClass('is-visible');
                }
            });

            function scrollToTop() {
                $body.addClass('blockSticky');
                var speed = $(window).scrollTop() / 4 > 500 ? $(window).scrollTop() / 4 : 500;
                if (isMobile) {
                    speed = speed * 2;
                }
                $("html, body").animate({
                    scrollTop: 0
                }, speed, function() {
                    $body.removeClass('blockSticky');
                });
                GOODWIN.stickyheader.destroySticky();
            }
            $button.on('click', function(e) {
                scrollToTop();
                e.preventDefault();
            });
            $('.logo-holder-s').on('click', function(e) {
                if (isMobile) {
                    scrollToTop();
                    e.preventDefault();
                }
            });
            if ($("body").hasClass("page-index")) {
                function subscriptionPopup() {
                    // get the mPopup
                    var mpopup = $("#mpopupBox");

                    // open the mPopup
                    mpopup.show();
                    $("body").addClass("open-newsletter");

                    // close the mPopup once close element is clicked
                    $(".close-model").on("click", function() {
                        mpopup.hide();
                        $("body").removeClass("open-newsletter");
                    });

                    // close the mPopup when user clicks outside of the box
                    $(window).on("click", function(e) {
                        if (e.target == mpopup[0]) {
                            mpopup.hide();
                        }
                    });
                }

                $(document).ready(function() {
                    var popDisplayed = $.cookie("popDisplayed");
                    if (popDisplayed == "1") {
                        return false;
                    } else {
                        setTimeout(function() {
                            subscriptionPopup();
                        }, 5000);
                        $.cookie("popDisplayed", null, {
                            path: "/"
                        });
                        // $.cookie('popDisplayed', '1', { expires: 7 });
                    }
                });
            }

            /* jQuery Cookie Plugin v1.3.1 */
            (function(a) {
                if (typeof define === "function" && define.amd) {
                    define(["jquery"], a);
                } else {
                    a(jQuery);
                }
            })(function(e) {
                var a = /\+/g;

                function d(g) {
                    return g;
                }

                function b(g) {
                    return decodeURIComponent(g.replace(a, " "));
                }

                function f(g) {
                    if (g.indexOf('"') === 0) {
                        g = g
                            .slice(1, -1)
                            .replace(/\\"/g, '"')
                            .replace(/\\\\/g, "\\");
                    }
                    try {
                        return c.json ? JSON.parse(g) : g;
                    } catch (h) {}
                }
                var c = (e.cookie = function(p, o, u) {
                    if (o !== undefined) {
                        u = e.extend({}, c.defaults, u);
                        if (typeof u.expires === "number") {
                            var q = u.expires,
                                s = (u.expires = new Date());
                            s.setDate(s.getDate() + q);
                        }
                        o = c.json ? JSON.stringify(o) : String(o);
                        return (document.cookie = [
                            c.raw ? p : encodeURIComponent(p),
                            "=",
                            c.raw ? o : encodeURIComponent(o),
                            u.expires ? "; expires=" + u.expires.toUTCString() : "",
                            u.path ? "; path=" + u.path : "",
                            u.domain ? "; domain=" + u.domain : "",
                            u.secure ? "; secure" : ""
                        ].join(""));
                    }
                    var g = c.raw ? d : b;
                    var r = document.cookie.split("; ");
                    var v = p ? undefined : {};
                    for (var n = 0, k = r.length; n < k; n++) {
                        var m = r[n].split("=");
                        var h = g(m.shift());
                        var j = g(m.join("="));
                        if (p && p === h) {
                            v = f(j);
                            break;
                        }
                        if (!p) {
                            v[h] = f(j);
                        }
                    }
                    return v;
                });
                c.defaults = {};
                e.removeCookie = function(h, g) {
                    if (e.cookie(h) !== undefined) {
                        e.cookie(
                            h,
                            "",
                            e.extend(g, {
                                expires: -1
                            })
                        );
                        return true;
                    }
                    return false;
                };
            });

            $(document).ready(function() {
                $("#chekout-by").click(function() {
                    console.log($("#variantId").val());
                    jQuery.ajax({
                        type: "POST",
                        url: "/cart/add.js",
                        data: {
                            quantity: 1,
                            id: $("#variantId").val()
                        },
                        dataType: "json",
                        success: function() {
                            window.location.href = "/checkout";
                        },
                        error: function(res) {
                            console.log(res);
                        }
                    });
                });

            });

            $(".product_input").on("change", function() {
                if ($("product_input:checked")) {
                    var data = $(this).data("title");
                    if (data == "MINI") {
                        $("#msg-1").css("display", "block");
                        $("#msg-2").css("display", "none");
                    } else {
                        $("#msg-2").css("display", "block");
                        $("#msg-1").css("display", "none");
                    }
                }
            });
            $("#forget").click(function() {
                $(".lgn").hide();
                $("#recover").show();
            });
            $(".cnl").click(function() {
                $("#recover").hide();
                $(".lgn").show();
            });
            $(".link").click(function() {
                $(".collapse").collapse("hide");
            });
            // PDP Qty Selector
            $("button.qty-btns").on("click", function(event) {
                event.preventDefault();
                var container = $(event.currentTarget).closest("[data-qtyContainer]");
                var qtEle = $(container).find("[data-qty]");
                var currentQty = $(qtEle).val();
                var qtyDirection = $(this).data("direction");
                var newQty = 0;

                if (qtyDirection == "1") {
                    newQty = parseInt(currentQty) + 1;
                } else if (qtyDirection == "-1") {
                    newQty = parseInt(currentQty) - 1;
                }

                if (newQty == 1) {
                    $(".decrement-quantity").attr("disabled", "disabled");
                }
                if (newQty > 1) {
                    $(".decrement-quantity").removeAttr("disabled");
                }

                if (newQty > 0) {
                    newQty = newQty.toString();
                    $(qtEle).val(newQty);
                } else {
                    $(qtEle).val("1");
                }
            });

            $(".popup_model").click(e => {
                var popup_value = $(e.target).data("vale");
                if (popup_value == "img") {
                    $("#max_width").css({
                        maxWidth: "500px"
                    });
                    $(".image_popup").css("display", "block");
                    $(".def_model").css("display", "none");
                } else if (popup_value == "membership") {
                    $("#max_width").css({
                        maxWidth: "80%"
                    });
                    $(".def_model").css("display", "block");
                    $(".image_popup").css("display", "none");
                }
            });

            $(".rc_custom_item_toggle").click(e => {
                var data = $(e.target).data("value");
                $(".rc_custom_item_toggle").removeClass("active");
                $(e.target).addClass("active");
                $("input[name='properties[shipping_interval_frequency]']").val(data);
            });

            // $(document).ready(function () {
            //   $('#AddToCart').on('click', function (event) {
            //     event.preventDefault();
            //     const $form = $(this).closest('form');
            //     Shopify.queue = [];
            //     const variantID = $form.find('[name="id"]').val();
            //     let Qty = parseInt($('[name="quantity"]').val());
            //     Shopify.queue.push({
            //       variantId: variantID,
            //       quantity: Qty
            //     });
            //     $('[data-queue]').each(function () {
            //       if ($(this).find('input[type="checkbox"]').prop('checked') == true) {
            //         let variantID = $(this).find('input[type="checkbox"]').val();
            //         Qty = 1;
            //         let properties = {};
            //         properties["Addons"] = true;

            //         Shopify.queue.push({
            //           variantId: variantID,
            //           quantity: Qty,
            //           properties: properties
            //         });
            //       }
            //     })
            //     console.log(Shopify.queue)
            //     Shopify.moveAlong();
            //   })

            //   Shopify.moveAlong = function () {
            //     if (Shopify.queue.length) {
            //       var request = Shopify.queue.shift();
            //       Shopify.addItem(request.variantId, request.quantity, request.properties, Shopify.moveAlong);
            //     } else {
            //       //Shopify.AjaxifyCart.init();
            //       document.location.href = '/cart';
            //     }
            //   };

            //   Shopify.addItem = function (id, qty, properties, callback) {
            //     var params = {
            //       quantity: qty,
            //       id: id
            //     };
            //     if (properties != false) {
            //       params.properties = properties;
            //     }
            //     $.ajax({
            //       type: 'GET',
            //       url: '/admin/api/2020-01/orders.json',
            //       dataType: 'json',
            //       success: function (data) {
            //         console.log(data)
            //       },
            //       error: function () {}
            //     });
            //   }
            // })
            $(".add-cart").click(function() {
                cartDrawer.offcanvas("open");
            });

            $(".card-link").click(function() {
                if (
                    $(this)
                    .parent()
                    .hasClass("active")
                ) {
                    $(this)
                        .parent()
                        .removeClass("active");
                    var p = $(this)
                        .parent()
                        .parent()
                        .parent();
                    $(p)
                        .children(".card")
                        .children(".card-header")
                        .removeClass("active");
                } else {
                    var p = $(this)
                        .parent()
                        .parent()
                        .parent();
                    $(p)
                        .children(".card")
                        .children(".card-header")
                        .removeClass("active");
                    $(this)
                        .parent()
                        .addClass("active");
                }
            });


        };
        custom.init = () => {
            cacheDom();
            bindUIActions();
        };
    })((window.custom = window.custom || {}), $);
};