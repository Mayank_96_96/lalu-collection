// import $ from 'jquery';
import slick from 'slick-carousel';

export default () => {
    (function(Slider, $) {

        const $dom = {};
        const arrow = '<svg fill="#959595" viewBox="0 0 129 129"><path d="M121.3 34.6c-1.6-1.6-4.2-1.6-5.8 0l-51 51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8 0-1.6 1.6-1.6 4.2 0 5.8l53.9 53.9c.8.8 1.8 1.2 2.9 1.2 1 0 2.1-.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2.1-5.8z"/></svg>'
        const cacheDom = () => {

            $dom.announcementSlider = $(".annucement-bar");
            $dom.bannerSlider = $(".banner-slide");
            $dom.testimonialSlider = $(".testimonial-slider");
            $dom.provenSlider = $(".proven-img-slider");
            $dom.pdpSlider = $(".product-slider");
            $dom.pdpSliderFor = $(".pro-nav-ul");
            $dom.skinSlider = $(".skin-image-slider");
            $dom.skinimgSliderFor = $(".skin-nav");
            $dom.relatedSlider = $(".related-product-slider");
            $dom.formulaSlider = $(".formula-slider");
            $dom.brandSlider = $(".slider_div");
            $dom.blogSlider = $(".blog_slider_main");
        };
        const announcementSlidersInit = () => {
            const announcementSliderOptions = {
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                nav: false,
                fade: true,
                arrows: false
            }
            $dom.announcementSlider.slick(announcementSliderOptions);
        };
        const brandSlidersInit = () => {
            const brandSliderOptions = {
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                nav: false,
                arrows: false,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 5
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            autoplay: true
                        }
                    }
                ]
            }
            $dom.brandSlider.slick(brandSliderOptions);
        };
        const blogSlidersInit = () => {
            const blogSliderOptions = {
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                nav: false,
                arrows: false,
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            autoplay: true
                        }
                    }
                ]
            }
            $dom.blogSlider.slick(blogSliderOptions);
        };
        const bannerSlidersInit = () => {
            const bannerSliderOptions = {
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                nav: false,
                arrows: false
            }
            $dom.bannerSlider.slick(bannerSliderOptions);
        };
        const provenSlidersInit = () => {
            const provenSliderOptions = {
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                // autoplay: true,
                arrows: true,
                autoplaySpeed: 2000,
                nav: false,
                dots: true,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            arrows: false,
                            // autoplay: true

                        }
                    }
                ]
            }
            $dom.provenSlider.slick(provenSliderOptions);
        };
        const SlidersInit = () => {
            const pdpSliderOptions = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                fade: true,
                asNavFor: $dom.pdpSliderFor
            }

            const pdpSliderSliderOptions = {
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: $dom.pdpSlider,
                dots: false,
                vertical: true,
                centerMode: true,
                arrows: false,
                focusOnSelect: true
            }

            $dom.pdpSlider.slick(pdpSliderOptions);
            $dom.pdpSliderFor.slick(pdpSliderSliderOptions);
        };

        const newtestiSlidersInit = () => {
            const testimonialSliderOptions = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                responsive: [{
                        breakpoint: 767,
                        settings: {
                            arrows: false,
                            autoplay: true,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            arrows: false,
                            autoplay: true
                        }
                    }
                ]
            }

            // const testimonialforSliderOptions = {
            //     slidesToShow: 1,
            //     slidesToScroll: 1,
            //     asNavFor: $dom.testimonialSlider,
            //     dots: false,
            //     arrows: false,
            //     fade: true,
            //     focusOnSelect: true,
            //     responsive: [
            //         {
            //             breakpoint: 767,
            //             settings: {
            //                 arrows: false,
            //                 autoplay: true
            //             }
            //         }
            //     ]
            // }

            $dom.testimonialSlider.slick(testimonialSliderOptions);
            // $dom.testimonialforSlider.slick(testimonialforSliderOptions);
        };

        const skincontentSlidersInit = () => {
            const skinSliderOptions = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                asNavFor: $dom.skinimgSliderFor,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            arrows: false,
                            autoplay: true
                        }
                    }
                ]
            }

            const skinimgSliderOptions = {
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: $dom.skinSlider,
                dots: false,
                centerMode: true,
                arrows: false,
                focusOnSelect: true

            }

            $dom.skinSlider.slick(skinSliderOptions);
            $dom.skinimgSliderFor.slick(skinimgSliderOptions);
        };

        const relatedSlidersInit = () => {
            const relatedSliderOptions = {
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                nav: false,
                arrows: true,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            arrows: false,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1
                        }
                    }
                ]
            }
            $dom.relatedSlider.slick(relatedSliderOptions);
        };

        const formulaSlidersInit = () => {
            const formulaSliderOptions = {
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                nav: false,
                arrows: false,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            slidesToShow: 1
                        }
                    }
                ]
            }
            $dom.formulaSlider.slick(formulaSliderOptions);
        };

        Slider.init = function() {
            cacheDom();
            announcementSlidersInit();
            brandSlidersInit();
            bannerSlidersInit();
            newtestiSlidersInit();
            provenSlidersInit();
            SlidersInit();
            skincontentSlidersInit();
            relatedSlidersInit();
            formulaSlidersInit();
            blogSlidersInit();
        };

    }(window.Slider = window.Slider || {}, $, undefined));
}